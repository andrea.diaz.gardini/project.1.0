import React, { Component } from 'react';
import LinesEllipsis from 'react-lines-ellipsis';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';

import './News.css';
import NewsSelect from './NewsSelect';

class NewsItem extends Component {
  state = {
    more: false,
    redirect: false
  }

  handleClick = btn => {
    this.setState({
      more: !this.state.more
    })
  }

  linkClick = () => {
    this.setState({
      redirect: true
    })
  }



  render(){
    if (this.state.redirect) {
      return <Redirect to={"/news/" + this.props.id}/> }

    console.log(this.state.redirect, this.props.id);
    return (
    <Router>
      <div className="news-card">
        <img  src={ this.props.img }
              onClick={ () => this.linkClick() }/>
        <h2 onClick={ () => this.linkClick() }>{ this.props.titulo }</h2>

        <p className={ this.state.more ?
                  "news-text" : "news-text-hide" }>
          <LinesEllipsis
          text={ this.props.texto }
          maxLine='4'
          ellipsis=' ...'
          trimRight
          basedOn='letters'
          />
        </p>
        <i className={ this.state.more ?
                  "far fa-bookmark save" : "news-text-hide" }></i>
        <i  className="fas fa-grip-lines"
            onClick={ () => this.handleClick({id: 1}) }></i>


      </div>
    </Router>
      )
    }
  }

  export default NewsItem;
