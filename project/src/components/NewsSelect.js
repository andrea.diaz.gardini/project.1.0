import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';

import './NewsSelect.css';
import { noticias } from './noticias.json';
import { users } from './users.json';

class NewsSelect extends Component {
  state = {
    noticias,
    users,
    redirect: false
  }

  linkClick = () => {
    this.setState({
      redirect: true
    })
  }


  render() {
    if (this.state.redirect) {
      return <Redirect to="/"/> }

    return(
      <div className="selected">
        <i  className="fas fa-arrow-left arrow"
            onClick={ () => this.linkClick() }></i>
        { this.state.noticias.filter(
            item => item.id == this.props.match.params.id).map (
              noticias  =>
              <div className="card">
                <img className="selected-img" src={"/" + noticias.img} />
                <h2> {noticias.titulo} </h2>
                <div className="icons">
                  <i className="far fa-heart heart">
                    <p>23</p>
                  </i>
                  <i className="far fa-comment-alt">
                    <p>12</p>
                  </i>
                  <i className="far fa-bookmark right"></i>
                  <i className="fas fa-share-alt right"></i>
                </div>
              </div> )
        }
        { this.state.noticias.filter(
            item => item.id == this.props.match.params.id).map (
              noticias  =>
              <div className="noticia">
                <p>{ noticias.fecha }</p>
                <p>{ noticias.texto }</p>
              </div> )
        }
        <div className="comment">
          { this.state.users.map(users =>
              <img className="avatar" src={"/" + users.avatar} />
            )
          }
          <input type="text"
          placeholder="Escribe un comentario"/>
          <i class="fas fa-caret-right"></i>
        </div>


      </div>
    );
  }
}

export default NewsSelect;
