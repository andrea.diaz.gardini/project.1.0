import React, { Component } from 'react';
import './BottomBar.css';

class BottomBar extends Component {
  render() {
    return (
      <div className="BottomBar">
        <a>
          <i className="fas fa-home"></i>
        </a>
        <a>
          <i className="fas fa-plus"></i>
        </a>
        <a>
          <i className="far fa-user"></i>
        </a>
        <a>
          <i className="far fa-bell"></i>
        </a>
        <a>
          <i className="fas fa-align-justify"></i>
        </a>
      </div>
    );
  }
}

export default BottomBar;
