import React, { Component } from 'react';

import './News.css';
import { noticias } from './noticias.json';
import NewsItem from './NewsItem'

class News extends Component {
  constructor() {
    super();
    this.state = {
      noticias
    }
  }

  render() {

    return (
      <div className="news">
        { this.state.noticias.map(noticias =>
          <NewsItem img={ noticias.img }
                    titulo={ noticias.titulo }
                    texto={ noticias.texto }
                    id={ noticias.id }
          />
          )
        }
      </div>

    )
  }
}

export default News;
