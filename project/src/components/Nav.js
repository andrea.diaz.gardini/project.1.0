import React, { Component } from 'react';

import './Nav.css';
import NavButton from './NavButton';

class Nav extends Component {
  state = {
    menu: [ {
          "id": 0,
          "title": "Todos"
        },
        {
          "id": 1,
          "title": "Institucional"
        },
        {
          "id": 2,
          "title": "Reconocimiento"
        },
        {
          "id": 3,
          "title": "Innovacion"
        }]
  }

  render() {

    return (
      <nav className="navbar">
        <div className="search">
          <input type="text"
          placeholder="¿Que deseas saber?"/>
          <i className="fas fa-search"></i>
        </div>
        <div className="content">
          { this.state.menu.map(menu =>
            <NavButton key={menu.id} text={menu.title} />)
          }
        </div>
      </nav>
    );
  }
}



export default Nav;
