import React, { Component } from 'react';

import '../App.css';

import BottomBar from './BottomBar';
import Nav from './Nav';
import News from './News';

class Home extends Component {
  render() {
    return(
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Noticias</h1>
          <Nav />
        </header>
        <div>
          <News />
        </div>
        <BottomBar />
      </div>
    );
  }
}

export default Home;
