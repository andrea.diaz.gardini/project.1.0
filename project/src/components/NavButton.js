import React, { Component } from 'react';

import './NavButton.css';

class NavButton extends Component {

  state = {
    active: false,
  }

  handleClick = btn => {
    this.setState({
      active: !this.state.active,
    })
  }

  render(){
    return (
      <a
        id= '1'
        className={ this.state.active ?
                  "content-link-click content-link" : "content-link" }
        onClick={ () => this.handleClick(this.props.text)}>{this.props.text}
        </a>
    )
  }
}

export default NavButton;
