import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import './App.css';

import Home from './components/Home';
import NewsSelect from './components/NewsSelect';


class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Route exact path="/" component={Home} />
          <Route path="/news/:id" exact strict component={NewsSelect} />
        </div>
      </Router>
    );
  }
}

export default App;
